# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

l = Leader.create(first_name: "Chicho", last_name: "Frumboli", sex: :male)
f = Follower.create(first_name: "Juana", last_name: "Sepulveda", sex: :female)
c = Couple.new
c.leader= l
c.follower = f
c.save!

