class CreateArtistsAndCouples < ActiveRecord::Migration
  def change
    create_table :artists_couples do |t|
      t.belongs_to :artist, index: true
      t.belongs_to :couple, index: true
    end
  end
end
