class AddLinkToCouple < ActiveRecord::Migration
  def change
    add_column :couples, :link, :string
  end
end
