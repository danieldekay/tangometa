class AddMoreMetadataToPerformance < ActiveRecord::Migration
  def change
    change_table :performances do |t|
      t.date :video_date
    end
  end
end
