class CreatePerformances < ActiveRecord::Migration
  def change
    create_table :performances do |t|
      t.date :date
      t.string :location
      t.string :link

      t.timestamps null: false
    end
  end
end
