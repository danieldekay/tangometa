class AddCouplesToPerformances < ActiveRecord::Migration
  def change
    change_table :performances do |t|
      t.belongs_to :couple
    end
  end
end
