class CreateMusics < ActiveRecord::Migration
  def change
    create_table :songs do |t|
      t.string :title
      t.string :orchestra

      t.timestamps null: false
    end

    add_column :dances, :song_id, :integer
  end
end
