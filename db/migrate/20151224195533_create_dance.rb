class CreateDance < ActiveRecord::Migration
  def change
    create_table :dances do |t|
      t.belongs_to :song, index: true
      t.belongs_to :couple, index: true
      t.date :date
      t.string :location
      t.string :link
      t.timestamps null: false
    end
  end
end
