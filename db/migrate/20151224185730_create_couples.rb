class CreateCouples < ActiveRecord::Migration
  def change
    create_table :couples do |t|
      t.belongs_to :leader
      t.belongs_to :follower
      t.date :start
      t.date :end

      t.timestamps null: false
    end
  end

end

