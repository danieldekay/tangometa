class AddMoreMetadataFields < ActiveRecord::Migration
  def change
    add_column :dances, :video_date, :date
    add_column :dances, :youtube_id, :string
    add_column :couples, :link, :string
  end
end
