require "test_helper"

class DanceCrudTest < ActiveSupport::TestCase
  describe "Create" do
    it "persists valid" do

      dance = Dance::Create.(dance:
          {
              couple_id: Couple.first.id,
              link: "http://www.youtube.de"
          }).model

      dance.persisted?.must_equal true
      dance.couple_id.must_equal Couple.first.id
      dance.link.must_equal "http://www.youtube.de"
    end

  end

end
