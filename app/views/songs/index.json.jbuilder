json.array!(@songs) do |music|
  json.extract! music, :id, :title, :orchestra
  json.url song_url(music, format: :json)
end
