json.array!(@performances) do |performance|
  json.extract! performance, :id, :date, :location, :link
  json.url performance_url(performance, format: :json)
end
