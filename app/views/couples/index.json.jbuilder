json.array!(@couples) do |couple|
  json.extract! couple, :id, :start, :end
  json.url couple_url(couple, format: :json)
end
