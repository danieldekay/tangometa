class DancesController < ApplicationController
  before_action :set_dance, only: [:show, :edit, :update, :destroy]
  respond_to :html

  # GET /dances
  # GET /dances.json
  def index
    @dances = Dance.all
  end

  # GET /dances/1
  # GET /dances/1.json
  def show
    if @dance.youtube_id
      @video = Yt::Video.new id:  @dance.youtube_id
    else
      @video = Yt::Video.new url:  @dance.link
    end

  end

  # GET /dances/new
  def new
    form Dance::Create

    # @dance = Dance.new
    # @dance.link = "https://www.youtube.com/watch?v=#{params[:video]}" rescue ""
    # @dance.youtube_id = params[:video] rescue nil
    # @dance.couple = Couple.find(params[:couple]) rescue nil
  end

  # GET /dances/1/edit
  def edit
      form Dance::Update
      render action: :new
  end

  # POST /dances
  # POST /dances.json
  def create
    #this is the new trailblazer code
    run Dance::Create do |op|
      return redirect_to op.model
    end
    render action: :new

    # @dance = Dance.new(dance_params)
    # @dance.youtube_id = YoutubeID.from(@dance.link)
    #
    # respond_to do |format|
    #   if @dance.save
    #     format.html { redirect_to @dance, notice: 'Dance was successfully created.' }
    #     format.json { render :show, status: :created, location: @dance }
    #   else
    #     format.html { render :new }
    #     format.json { render json: @dance.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PATCH/PUT /dances/1
  # PATCH/PUT /dances/1.json
  def update

    @dance.youtube_id = YoutubeID.from(params[:dance][:link])
    @video = Yt::Video.new url: params[:dance][:link]
    @dance.video_date = @video.published_at

    respond_to do |format|
      if @dance.update(dance_params)
        format.html { redirect_to @dance, notice: 'Dance was successfully updated.' }
        format.json { render :show, status: :ok, location: @dance }
      else
        format.html { render :edit }
        format.json { render json: @dance.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dances/1
  # DELETE /dances/1.json
  def destroy
    @dance.destroy
    respond_to do |format|
      format.html { redirect_to dances_url, notice: 'Dance was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dance
      @dance = Dance.find(params[:id])
    end

    #Never trust parameters from the scary internet, only allow the white list through.
    def dance_params
       params.require(:dance).permit(:date, :location, :link, :couple_id, :song_id, :video_date)
     end

    def update_youtube_id
      self.youtube_id = YoutubeID.from(self.link)
    end
end
