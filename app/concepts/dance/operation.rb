class Dance < ActiveRecord::Base
  class Create < Trailblazer::Operation
    include Model
    model Dance, :create

    contract do
      property :couple_id
      property :link
      property :location
      property :song_id
      property :date

      validates :couple_id, presence: true
      validates :link, presence: true
    end

    def process(params)
      validate(params[:dance]) do |f|
        f.save
      end
    end
  end
  class Update < Create
    action :update

    contract do

    end
  end

end
