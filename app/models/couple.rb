class Couple < ActiveRecord::Base
  belongs_to :leader
  belongs_to :follower

  has_many :dances

  def name
    follower.name + " & ".html_safe + leader.name
  end

=begin
  def follower
    Artist.find(follower_id)
  end

  def leader
    Artist.find(leader_id)
  end
=end

end
