class Follower < Artist
  has_many :couples, :foreign_key => :follower_id
end
