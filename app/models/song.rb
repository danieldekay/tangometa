class Song < ActiveRecord::Base
  has_many :dances

  def to_s
    "''" + title.titleize + "'' by " + orchestra
  end

end
