class Leader < Artist
  has_many :couples, :foreign_key => :leader_id
end
