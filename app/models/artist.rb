class Artist < ActiveRecord::Base
  has_many :couples

  def name
    first_name + " " + last_name
  end
end

